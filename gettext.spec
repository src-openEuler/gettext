%bcond_with jar
%bcond_with java
%bcond_without check

Name:    gettext
Version: 0.24
Release: 1
License: GPL-3.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-or-later
Summary: GNU gettext utilities are a set of tools that provides a framework to help other GNU packages produce multi-lingual messages
URL:     https://www.gnu.org/software/gettext/
Source:  https://ftp.gnu.org/pub/gnu/gettext/%{name}-%{version}.tar.xz
Patch2: gettext-0.21.1-covscan.patch

BuildRequires: gcc-c++, emacs, xz, chrpath, autoconf, automake
BuildRequires: ncurses-devel, libxml2-devel, glib2-devel, libunistring-devel
%if %{with java}
BuildRequires: gcc-java, libgcj
BuildRequires: java-1.6.0-openjdk-devel
%if %{with jar}
BuildRequires: %{_bindir}/fastjar
BuildRequires: zip, unzip
%endif
%endif
Conflicts: filesystem < 3
Provides: /bin/gettext
Provides: bundled(gnulib)
Provides: bundled(libcroco) = 0.6.12
Requires: %{name}-runtime = %{version}-%{release}
Requires: libtextstyle%{?_isa} = %{version}-%{release}

%description
GNU gettext is an important step for the GNU Translation Project, as it
is an asset on which we may build many other steps. This package offers
to programmers, translators, and even users, a well integrated set of
tools and documentation. Specifically, the GNU gettext utilities are a
set of tools that provides a framework to help other GNU packages
produce multi-lingual messages. These tools include a set of conventions
about how programs should be written to support message catalogs, a
directory and file naming organization for the message catalogs themselves,
a runtime library supporting the retrieval of translated messages, and
a few stand-alone programs to massage in various ways the sets of
translatable strings, or already translated strings. A special GNU Emacs
mode also helps interested parties in preparing these sets, or bringing
them up to date.

%package runtime
Summary: GNU runtime libraries and programs for producing multi-lingual messages
License: GPL-3.0-or-later and LGPL-2.0-or-later
# Depend on the exact version of the library sub package
Requires: %{name}-libs%{_isa} = %{version}-%{release}
Requires: %{name}-envsubst = %{version}-%{release}
Conflicts: %{name} < 0.23.1

%description runtime
The GNU gettext-runtime package provides an easy to use runtime libraries and
programs for creating, using, and modifying natural language catalogs
and is a powerful and simple method for internationalizing programs.

%package devel
Summary: Development files for %{name}
License: LGPL-2.0-or-later AND GPL-3.0-or-later AND GFDL-1.2-or-later
Requires: %{name} = %{version}-%{release}
Requires: xz
Obsoletes: gettext-autopoint < 0.18.1.1-3
Provides: gettext-autopoint = %{version}-%{release}
Obsoletes: %{name}-common-devel < %{version}-%{release}
Provides: %{name}-common-devel = %{version}-%{release}

%description devel
This package provides development files and tools which help edit/alter
po files, if you need to compile gettext related application/library or
edit/alter po files, you should install this package.
want to add gettext support for your project.

%package libs
Summary: Libraries for %{name}
# libasprintf is LGPLv2+
# libgettextpo is GPLv3+
License: LGPL-2.0-or-later and GPL-3.0-or-later
Requires: libtextstyle%{?_isa} = %{version}-%{release}
Conflicts: gettext%{?_isa} < 0.23.1

%description libs
This package contains libraries used internationalization support.

%package -n libtextstyle
Summary: Text styling library
License: GPL-3.0-or-later

%description -n libtextstyle
Library for producing styled text to be displayed in a terminal
emulator.

%package -n libtextstyle-devel
Summary: Development files for libtextstyle
License: GPL-3.0-or-later and GFDL-1.2-or-later
Requires: libtextstyle%{?_isa} = %{version}-%{release}

%description -n libtextstyle-devel
This package contains all development related files necessary for
developing or compiling applications/libraries that needs text
styling.

%package -n emacs-%{name}
Summary: Support for editing po files within GNU Emacs
Group: Application/Editors
BuildArch: noarch
# help users find po-mode.el
Provides: emacs-po-mode
Requires: emacs(bin) >= %{_emacs_version}
Provides: emacs-%{name}-el = %{version}-%{release}
Obsoletes: emacs-%{name}-el < %{version}-%{release}

%description -n emacs-%{name}
This package provides a major mode for editing po files within GUN Emacs

%package envsubst
Summary: Substitutes the values of environment variables
Conflicts: %{name} < 0.23.1

%description envsubst
Substitutes the values of environment variables.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

autoreconf

# Defeat libtextstyle attempt to bundle libxml2.  The comments
# indicate this is done because the libtextstyle authors do not want
# applications using their code to suffer startup delays due to the
# relocations.  This is not a sufficient reason for Fedora.
sed -e 's/\(gl_cv_libxml_force_included=\)yes/\1no/' \
    -i libtextstyle/configure

%build
%if %{with java}
export JAVAC=gcj
%if %{with jar}
export JAR=fastjar
%endif
%endif
%ifarch ppc ppc64 ppc64le
export CFLAGS="%{build_cflags} -D__SUPPORT_SNAN__"
%endif
export CPPFLAGS="%{build_cflags} -I%{_includedir}/libxml2"
# Side effect of unbundling libxml2 from libtextstyle.
export LIBS="-lxml2"
export CFLAGS="%{build_cflags} $CFLAGS -Wformat"
%configure --enable-nls --disable-static \
    --enable-shared --with-pic --disable-csharp --disable-rpath \
%if %{with java}
    --enable-java \
%else
    --disable-java --disable-native-java \
%endif
    --with-xz

# Eliminate hardcoded rpaths
sed -e 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' \
    -e 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' \
    -i $(find . -name libtool)

%make_build %{?with_java:GCJFLAGS="-findirect-dispatch"}

%install
%make_install \
    lispdir=%{_datadir}/emacs/site-lisp/gettext \
    aclocaldir=%{_datadir}/aclocal EXAMPLESFILES=""

%delete_la

# make preloadable_libintl.so executable
chmod 755 ${RPM_BUILD_ROOT}%{_libdir}/preloadable_libintl.so

# doc relocations
for i in gettext-runtime/man/*.html; do
  rm ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/`basename $i`
done
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/javadoc*
rm -rf ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/examples

rm -rf htmldoc
mkdir htmldoc
mv ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext/* ${RPM_BUILD_ROOT}/%{_datadir}/doc/libasprintf/* htmldoc
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/libasprintf
rm -r ${RPM_BUILD_ROOT}%{_datadir}/doc/gettext

%if %{with jar}
%else
rm -f ${RPM_BUILD_ROOT}%{_datadir}/%{name}/libintl.jar
%endif

rm -f ${RPM_BUILD_ROOT}%{_datadir}/%{name}/gettext.jar

mkdir -p $RPM_BUILD_ROOT%{_datadir}/%{name}/its

rm ${RPM_BUILD_ROOT}%{_libdir}/libgettext{src,lib}.so

install -d ${RPM_BUILD_ROOT}%{_emacs_sitestartdir}
mv ${RPM_BUILD_ROOT}%{_emacs_sitelispdir}/%{name}/start-po.el ${RPM_BUILD_ROOT}%{_emacs_sitestartdir}
rm ${RPM_BUILD_ROOT}%{_emacs_sitelispdir}/%{name}/start-po.elc

%find_lang %{name}-runtime
%find_lang %{name}-tools

%if %{with check}
%check
export LD_LIBRARY_PATH=$RPM_BUILD_ROOT%{_libdir}:$PWD/gettext-runtime/intl/.libs
make check LIBUNISTRING=-lunistring || true
%endif

%files -f %{name}-tools.lang
%doc AUTHORS NEWS README THANKS
%doc gettext-tools/misc/disclaim-translations.txt
%doc gettext-tools/man/msg*.1.html
%doc gettext-tools/man/recode*.1.html
%doc gettext-tools/man/xgettext.1.html
%doc gettext-tools/doc/FAQ.html
%doc gettext-tools/doc/tutorial.html
%{_bindir}/msgattrib
%{_bindir}/msgcat
%{_bindir}/msgcmp
%{_bindir}/msgcomm
%{_bindir}/msgconv
%{_bindir}/msgen
%{_bindir}/msgexec
%{_bindir}/msgfilter
%{_bindir}/msgfmt
%{_bindir}/msggrep
%{_bindir}/msginit
%{_bindir}/msgmerge
%{_bindir}/msgunfmt
%{_bindir}/msguniq
%{_bindir}/recode-sr-latin
%{_bindir}/xgettext
%if %{with java}
%exclude %{_libdir}/%{name}/gnu.gettext.*
%endif
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/its
%{_datadir}/%{name}/ABOUT-NLS
%{_datadir}/%{name}/po
%{_datadir}/%{name}/styles
%{_datadir}/%{name}/disclaim-translations.txt
%dir %{_datadir}/%{name}-%{version}
%{_datadir}/%{name}-%{version}/its
%dir %{_datadir}/%{name}/schema
%{_datadir}/%{name}/schema/its*.xsd*
%{_datadir}/%{name}/schema/locating-rules.xsd*
%dir %{_libexecdir}/%{name}
%{_libexecdir}/%{name}/cldr-plurals
%{_libexecdir}/%{name}/hostname
%{_libexecdir}/%{name}/project-id
%{_libexecdir}/%{name}/urlget
%{_libexecdir}/%{name}/user-email

%files runtime -f %{name}-runtime.lang
%license COPYING
%doc gettext-runtime/BUGS
%doc gettext-runtime/man/gettext.1.html
%doc gettext-runtime/man/ngettext.1.html
%doc gettext-runtime/intl/COPYING*
%{_bindir}/gettext
%{_bindir}/gettext.sh
%{_bindir}/ngettext

%files envsubst
%license COPYING
%doc gettext-runtime/man/envsubst.1.html
%{_bindir}/envsubst

%files devel
%doc gettext-runtime/man/*.3.html ChangeLog
%doc gettext-tools/man/autopoint.1.html
%doc gettext-tools/man/gettextize.1.html
%{_bindir}/autopoint
%{_bindir}/gettextize
%{_mandir}/man1/autopoint.1*
%{_datadir}/%{name}/projects/
%{_datadir}/%{name}/config.rpath
%{_datadir}/%{name}/*.h
%{_datadir}/%{name}/msgunfmt.tcl
%{_datadir}/aclocal/*
%{_includedir}/autosprintf.h
%{_includedir}/gettext-po.h
%{_infodir}/autosprintf*
%{_libdir}/libasprintf.so
%{_libdir}/libgettextpo.so
%{_libdir}/preloadable_libintl.so
%{_datadir}/%{name}/javaversion.class
%{_datadir}/%{name}/archive.*.tar.xz
%doc gettext-runtime/intl-java/javadoc*
%if %{with java}
%{_libdir}/%{name}/gnu.gettext.*
%endif

%files libs
%{_libdir}/libasprintf.so.0*
%{_libdir}/libgettextpo.so.0*
%{_libdir}/libgettextlib-0.*.so
%{_libdir}/libgettextsrc-0.*.so
%if %{with jar}
%{_datadir}/%{name}/libintl.jar
%endif

%files -n libtextstyle
%{_libdir}/libtextstyle.so.0*

%files -n libtextstyle-devel
%{_docdir}/libtextstyle/
%{_includedir}/textstyle/
%{_includedir}/textstyle.h
%{_infodir}/libtextstyle*
%{_libdir}/libtextstyle.so

%files -n emacs-%{name}
%dir %{_emacs_sitelispdir}/%{name}
%{_emacs_sitelispdir}/%{name}/*.elc
%{_emacs_sitelispdir}/%{name}/*.el
%{_emacs_sitestartdir}/*.el

%files help
%{_infodir}/gettext*
%{_mandir}/man1/*
%{_mandir}/man3/*
%exclude %{_mandir}/man1/autopoint.1*

%changelog
* Wed Feb 26 2025 Funda Wang <fundawang@yeah.net> - 0.24-1
- update to 0.24

* Wed Jan 01 2025 Funda Wang <fundawang@yeah.net> - 0.23.1-1
- update to 0.23.1
- reorganize sub packages according to upstream

* Sat Nov 30 2024 Funda Wang <fundawang@yeah.net> - 0.22.5-2
- fix build with latest gnulib
- cleanup spec

* Tue Jul 16 2024 dillon chen <dillon.chen@gmail.com> - 0.22.5-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 0.22.5

* Tue Feb 27 2024 fuanan  <fuanan3@h-partners.com> - 0.22-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Fix build failure

* Thu Jul 13 2023 dillon chen <dillon.chen@gmail.com> - 0.22-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 0.22

* Fri Nov 04 2022 fuanan <fuanan3@h-partners.com> - 0.21.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update version to 0.21.1

* Fri Sep 9 2022 yixiangzhike<yixiangzhike007@163.com> - 0.21-6
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add version number for Obsoletes and Provides

* Sat Jul 31 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.21-5
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Fri May 14 2021 yangzhuangzhuang<yangzhuangzhuang1@Huawei.com> - 0.21-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:detach the sub package emacs-gettext from gettex-devel

* Fri Apr 30 2021 yangzhuangzhuang<yangzhuangzhuang1@Huawei.com> - 0.21-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Add version number for Obsoletes: emacs-gettext

* Fri Dec 4 2020 panxiaohe<panxiaohe@huawei.com> - 0.21-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:Delete libcroco dependency and bundle it

* Tue Sep 1 2020 wangchen<wangchen137@huawei.com> - 0.21-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.21

* Mon Jul 27 2020 yang_zhuang_zhuang<yangzhuangzhuang1@huawei.com> - 0.20.2-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.20.2

* Tue Jan 7 2020 chengquan<chengquan3@huawei.com> - 0.20.1-2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fixbug in main package and help package

* Fri Oct 11 2019 hanzhijun<hanzhijun1@huawei.com> - 0.20.1-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update to 0.20.1

* Mon Sep 30 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.19.8.1-20
- modify CVE patch name

* Fri Sep 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.19.8.1-19
- fix CVE-2018-18751

* Tue Sep 17 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.19.8.1-18
- Package init
